// Codice per Arduino 1

void setup(){
    pinMode(3,INPUT); //Sasso P1
    pinMode(4,INPUT); //Carta P1
    pinMode(5,INPUT); //Forbici P1
    pinMode(6,OUTPUT); //Sasso output P1
    pinMode(7,OUTPUT); //Carta output P1
    pinMode(8,OUTPUT); //Forbici output P1
    Serial.begin(9600); 
}

void loop(){
    //digitalWrite(3,LOW);
    //digitalWrite(4,LOW);
    //digitalWrite(5,LOW);
    digitalWrite(6,LOW);
    digitalWrite(7,LOW);
    digitalWrite(8,LOW);

    if(digitalRead(3)==HIGH){
        digitalWrite(6,HIGH);
        Serial.print("Sasso");
        delay(5000);  
    }
    else{
        digitalWrite(6,LOW);
    }
    if(digitalRead(4)==HIGH){
        digitalWrite(7,HIGH);
        Serial.print("Carta");
        delay(5000);  
    }
    else{
        digitalWrite(7,LOW);
    }
    if(digitalRead(5)==HIGH){
        digitalWrite(8,HIGH);
        Serial.print("Forbici");
        delay(5000);  
    }
    else{
        digitalWrite(8,LOW);
    } 
}